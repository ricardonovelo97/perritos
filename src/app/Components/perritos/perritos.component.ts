import { Component, OnInit } from '@angular/core';

interface IDataResponse{
  perrito_url: string;
}

interface IApiResponse{
  message: string;
  status: string;
}

@Component({
  selector: 'app-perritos',
  templateUrl: './perritos.component.html',
  styleUrls: ['./perritos.component.scss'],
})
export class PerritosComponent implements OnInit {

  PerritosData: IDataResponse;
  constructor() { }

  ngOnInit() {
    this.PerritosData = <IDataResponse>{};
    this.getPerritosData();
  }

  getPerritosData(){
    fetch(`https://dog.ceo/api/breeds/image/random`)
    .then(response => response.json())
    .then((data: IApiResponse) => {
    this.PerritosData.perrito_url = data.message;
    });
  }

}
